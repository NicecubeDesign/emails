﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="what-is-subteacher.aspx.cs" Inherits="ResourcesManager.what_is_subteacher" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Src="~/Login.ascx" TagPrefix="uc1" TagName="Login" %>
<%@ Register Src="~/Promo.ascx" TagPrefix="uc1" TagName="Promo" %>
<%@ Register Src="~/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>What is SubTeacher.ie? | Connecting Teachers with Schools</title>
<link href="css/subteacher.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/orbit-1.2.3.css" />
<link rel="shortcut icon" href="favicon.png" />
<!-- Attach necessary JS -->
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.orbit-1.2.3.min.js"></script>	
		
			<!--[if IE]>
			     <style type="text/css">
			         .timer { display: none !important; }
			         div.caption { background:transparent; filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000,endColorstr=#99000000);zoom: 1; }
			    </style>
			<![endif]-->
	<script type="text/javascript">
        $(document).ready(function() {
        // Tooltip only Text
        $('.masterTooltip').hover(function(){
                // Hover over code
                var title = $(this).attr('title');
                $(this).data('tipText', title).removeAttr('title');
                $('<p class="tooltip"></p>')
                .text(title)
                .appendTo('body')
                .fadeIn('slow');
        }, function() {
                // Hover out code
                $(this).attr('title', $(this).data('tipText'));
                $('.tooltip').remove();
        }).mousemove(function(e) {
                var mousex = e.pageX - 50; //Get X coordinates
                var mousey = e.pageY + 10; //Get Y coordinates
                $('.tooltip')
                .css({ top: mousey, left: mousex })
        });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">

        <uc1:Promo runat="server" ID="Promo" />

        <div id="home-wrapper">
	        <div class="home-head-container">
            <uc1:Login runat="server" ID="Login" />
   	          <div class="home-banner-top-container">
      	        <div class="home-banner-top">
                    <div class="banner-top-left"><a href="default.aspx"><img src="images/logo.png" alt="SubTeacher.ie" width="358" height="65" /></a><a href="#"></a></div>
       	          <div class="banner-top-right">
          	        <div class="register-login"><a href="~/Account/Register.aspx" runat="server" id="RegisterBtn"><img src="images/but-teachers-register.png" alt="Teachers: Register here" width="198" height="65" /></a><a href="#"></a></div>
                    <div class="chalk"><img src="images/chalky.png" width="40" height="143" alt="" /></div>
                    <div class="register-login"><a href="~/Schools/RegisterSchool.aspx" runat="server" id="RegisterSchoolBtn"><img src="images/but-schools-register.png" alt="Login" width="198" height="65" /></a><a href="#"></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="body-content-container">
              <div class="body-content">
      	        <div class="body-content-left">
      	          <h1>What is SubTeacher?</h1>
      	          <p><img src="images/subteacher-infographic.png" alt="What is SubTeacher?" width="278" height="417" /></p>
      	        </div>
                <div class="body-content-right"> 
                  <h1 class="h1green">The Monday Morning Predicament </h1>
                  <ul>
                    <li><strong>8.00 am</strong> - School Receives call from Teacher calling in sick. Schools needs to find an available substitute Teacher as quick as possible.</li>
                    <li><strong>8.05 am</strong> - School logs on to SubTeacher.ie, within 3 clicks they can see available Teachers and make a job offer for the postion.</li>
                    <li><strong>8.10 am</strong> - Teacher receives and responds to Job offer in app or online.</li>
                    <li><strong>8.15 am</strong> - School has their vacant position filled, Teacher has received a job offer.</li>
                    <li><strong>8.20 am</strong> - Teacher leaves for work.</li>
                  </ul>
                  <h2>The Process is simple for both Teachers &amp; Schools...</h2>
                  <p><img src="images/what-is-subteacher-steps.png" width="620" height="153" alt="The SubTeacher process is simple for both Teachers &amp; Schools " /></p>
        <strong>SubTeacher</strong> is an online system which connects school Principals and Deputies with Teachers to fill substitute teaching positions.
                  <h3>Within 3 clicks, a School can find available Teachers to fill the substitute positions they have...</h3>
                </div>
              </div>
              </div>
      
              <uc1:Footer runat="server" ID="Footer" />
      
            </div>

        </div><!-- HOME WRAPPER END -->


    </form>
</body>
</html>
